# a is a list. The function gives the maximum value starting from the index i
def maximum(a, i):
    if len(a) == i:
        return a[-1]
    max = maximum(a, i + 1)
    if a[i] > max:
        return a[i]
    else:
        return max

# a is a list. The funcion reverses the elements between the indexes i and j
def reverse(a, i, j):
    if i >= j:
        return
    reverse(a, i + 1, j - 1)
    a[i], a[j] = a[j], a[i]

# s is a string. The function tells if the string between the indexes i and j is a palindrome
def isPalindrome(s, i, j):
    if i >= j:
        return True
    is_true = isPalindrome(s, i + 1, j - 1)
    if is_true:
        if s[i] == s[j]:
            return True
        else:
            return False
    else:
        return False

# a is a sorted list. The function finds a number in the list using binary search
def binarySearch(a, i, j, x):
    mid = (i + j) // 2
    if x == a[mid]:
        return True
    elif a[j] >= x > a[mid]:
        return binarySearch(a, mid + 1, j, x)
    elif a[i] <= x < a[mid]:
        return binarySearch(a, i, mid - 1, x)
    else:
        return False

# n and k are natural numbers with 0 <= n <=k. Finds the Binomial Coefficient (n,k)
def binomial(n, k):
    if k == n or k == 0:
        return 1
    return binomial(n - 1, k - 1) + binomial(n - 1, k)
