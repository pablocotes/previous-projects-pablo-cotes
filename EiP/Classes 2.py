class Punk2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __lt__(self, other):
        if self.x == other.x:
            return self.y < other.y
        return self.x < other.x


class Punkt3D:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __lt__(self, other):
        if self.x == other.x:
            return self.y < other.y
        return self.x < other.x


class PunktWolke:
    def __init__(self, punkte):
        self.p = punkte

    def bereich(self, low, high):
        liste = []
        punkt = 0 + self.p.y
        for i in range(0, punkt + 1):
            for j in range(low, high + 1):
                liste.append(Punkt3D(i, j))
        liste.sort()
        return liste
