# Implementieren Sie in dieser Datei ein Programm, das einen Startwert von der Konsole einliest und anschließend
# die Collatz-Folge zu diesem Startwert bis zum ersten auftreten von 1 als Folgendglied auf der Konsole ausgibt.

a0 = int(input('Geben Sie eine beliebige natürliche Zahl ein: '))
x = 2

while a0 != 1:
    if a0 % x == 0:
        a0 = a0 // 2
    else:
        a0 = 3 * a0 + 1
    print(a0)