class Intervall:
    def __init__(self, l, r):
        self.u = l  # u ==> untere
        self.o = r  # o ==> obere

    def length(self):
        return self.o - self.u

    def mid(self):
        return (self.u + self.o) / 2

    def contains(self, x):
        if self.u <= x <= self.o:
            return True
        return False

    def und(self, I):
        if self.u < I.u:
            untere = I.u
        else:
            untere = self.u
        if self.o < I.o:
            obere = self.o
        else:
            obere = I.o
        return Intervall(untere, obere)

    def oder(self, I):
        if self.u < I.u:
            untere = self.u
        else:
            untere = I.u
        if self.o < I.o:
            obere = I.o
        else:
            obere = self.o
        return Intervall(untere, obere)


class Rechteck:
    def __init__(self, X, Y):
        self.distance = X
        self.height = Y

    def area(self):
        return self.distance.length() * self.height.length()

    def perim(self):
        return 2 * (self.distance.length() + self.height.length())

    def mid(self):
        return [self.distance.mid(), self.height.mid()]

    def contains(self, x, y):
        if not self.distance.contains(x) or not self.height.contains(y):
            return False
        return True

    def und(self, R):
        return Rechteck(self.distance.und(R.distance), self.height.und(R.height))

    def oder(self, R):
        return Rechteck(self.distance.oder(R.distance), self.height.oder(R.height))