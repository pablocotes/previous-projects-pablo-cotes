def pascal(n):
    if n == 0:
        return [[1]]
    elif n == 1:
        return [[1], [1, 1]]
    temp = []
    liste = [[1], [1, 1]]
    for i in range(2, n + 1):
        temp.append(1)
        for j in range(1, i):
            temp.append(liste[i - 1][j] + liste[i - 1][j - 1])
        temp.append(1)
        liste.append(temp)
        temp = []
    return (liste)


def printpascal(n):
    ausgabe = ''
    for i in range(len(n)):
        for j in range(len(n[i])):
            ausgabe += str(n[i][j]) + '   '
        ausgabe += '\n'
    return print(ausgabe)


n = int(input('n = '))
print(pascal(n))
printpascal(pascal(n))