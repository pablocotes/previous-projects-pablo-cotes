# At the organic market, a mango costs 10 euros, an avocado costs 3 euros and a strawberry costs 0.50 euros. You buy
# exactly 100 pieces of fruit for exactly 100 Euro. How many mangoes, avocados and strawberries did you buy then?

mango = 10  # Preis Mangos
avocado = 3  # Preis Avocados
erdbeer = 0.5  # Preis 2 Erdbeere
obst = 0  # Anzahl Obst
preis = 0  # Gesamte Preis
antworten = 0  # Anzahl mögliche Antworten

anzahlmango = (100 // mango) + 1
anzahlavocado = (100 // avocado) + 1
anzahlerdbeere = (100 // erdbeer) + 1

for i in range(anzahlmango):
    for j in range(anzahlavocado):
        for k in range(int(anzahlerdbeere)):
            preis = i * 10 + j * 3 + k * 0.5
            obst = i + j + k
            if obst == 100 and preis == 100:
                print('Mangos: ', i, ' Avocados: ', j, ' Strawberries: ', k * 2)
                print('Price Mangos: ', i * 10, ' Price Avocados: ', j * 3, ' Price Strawberries: ', k)
                antworten += 1

if antworten == 1:
    print('There is only one way that the conditions can be met')
else:
    print('There are', antworten,
          " ways to meet the conditions and therefore isn't a conclusive answer")
