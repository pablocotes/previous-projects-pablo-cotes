# Solve the Tower of Hanoi for 7 levels

def printStapel(stapel):
    for S in stapel:
        for x in S:
            print(x, end=' ')
        print()
    print('------------------')


def hanoi3(n, start, ziel, hilf):
    global zugzahl
    if n == 0:
        return
    elif hilf != 1:
        hanoi3(n - 1, start, hilf, ziel)
        x = stapel[start].pop()
        stapel[ziel].append(x)
        zugzahl += 1
        printStapel(stapel)
        print(start, ' -> ', ziel)
        hanoi3(n - 1, hilf, ziel, start)
    else:
        hanoi3(n - 1, start, hilf, ziel)
        hanoi3(n - 1, hilf, ziel, start)
        x = stapel[start].pop()
        stapel[hilf].append(x)
        zugzahl += 1
        hanoi3(n - 1, ziel, hilf, start)
        hanoi3(n - 1, hilf, start, ziel)
        x = stapel[hilf].pop()
        stapel[ziel].append(x)
        zugzahl += 1
        hanoi3(n - 1, start, hilf, ziel)
        hanoi3(n - 1, hilf, ziel, start)


n = 7
zugzahl = 0
stapel = [[i for i in range(n, 0, -1)], [], []]
printStapel(stapel)

# bewege von Stab 0 nach Stab 2
hanoi3(n, 0, 2, 1)
print('Zugzahl = ', zugzahl)