# The advertising agencies Pfennigfuchs and Groschenluchs have merged. In a first step, the company now wants to find
# out how the entire data set with personal data has grown as a result. For an initial comparison, the two companies
# have exported the telephone numbers of all their customers into a text file. Read the contents of the two files into
# one list each. Then determine a third list that forms the intersection of the two lists, i.e. contains all telephone
# numbers that appear in both lists.
import time

t0 = time.time()
gros = []
pfe = []
beide = []
temp = []

numgros = 0
numpfe = 0

datei_1 = open('groschenluchs.txt', 'r', encoding='utf-8')

y = 0
for i in datei_1:
    y = i.split('\n')
    gros.append(y[0])

datei_1.close()

datei_2 = open('pfennigfuchs.txt', 'r', encoding='utf-8')

y = 0
for i in datei_2:
    y = i.split('\n')
    pfe.append(y[0])

datei_2.close()

gros.sort()
pfe.sort()

while numgros < len(gros) and numpfe < len(pfe):
    if gros[numgros] < pfe[numpfe]:
        numgros += 1
    elif gros[numgros] > pfe[numpfe]:
        numpfe += 1
    else:
        beide.append(gros[numgros])
        if numgros == len(gros) - 1 or numpfe == len(pfe) - 1:
            break
        elif gros[numgros + 1] < pfe[numpfe + 1]:
            numpfe += 1
        elif gros[numgros + 1] > pfe[numpfe + 1]:
            numgros += 1
        else:
            numgros += 1
            numpfe += 1

print('Elemente in Schnittmenge: ', len(beide))
print('Elemente die Vereinigung: ', len(gros) + len(pfe) - len(beide))
t1 = time.time()
print('Laufzeit : ', t1 - t0)
