def pascal(n):
    if n == 0:
        return [1]
    elif n == 1:
        return [1, 1, 1]
    altepascal = [1, 1]
    liste = [1, 1, 1]
    for i in range(2, n + 1):
        neuepascal = []
        neuepascal.append(1)
        for j in range(1, i):
            neuepascal.append(altepascal[j] + altepascal[j - 1])
        neuepascal.append(1)
        altepascal = [] + neuepascal
        for k in range(len(neuepascal)):
            liste.append(neuepascal[k])
    liste.append(n)
    return liste


def printpascal(n):
    ausgabe = ''
    x = 0
    for i in range(n[-1] + 1):
        for j in range(x, i + x + 1):
            ausgabe += str(n[j]) + '   '
        ausgabe += '\n'
        x += i + 1
    return print(ausgabe)


n = int(input('n = '))
print(pascal(n))
printpascal(pascal(n))