# Find all the IBANs that begin with
# DE68 2105 0170 0012 34xx xx


def rotate(text, d):
    textfirst = text[:d]
    textsecond = text[d:]
    text_out = textsecond + textfirst
    return text_out


def letter_to_number(text):
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    text = list(text)
    for letter in text:
        if letter in alphabet:
            num_of_let = str(alphabet.index(letter) + 10)
            text[text.index(letter)] = num_of_let

    return "".join(text)


def check_iban(text):
    if int(letter_to_number(rotate(text, 4))) % 97 == 1:
        return True
    else:
        return False


def autoload(text):
    print("Gültige IBAN Nummern für " + text + "XXXX wären: ")
    for i in range(10):
        for j in range(10):
            for k in range(10):
                for l in range(10):
                    if check_iban(text + str(i) + str(j) + str(k) + str(l)):
                        print(text + str(i) + str(j) + str(k) + str(l))


autoload("DE6821050170001234")
