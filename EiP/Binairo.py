# Solving the following binairo puzzles
# Information about the rules in www.puzzle-binairo.com
# The 2 represents an empty space
s1 = [[2, 2, 2, 1, 2, 2],
      [0, 2, 0, 2, 2, 2],
      [2, 1, 2, 1, 2, 1],
      [2, 2, 2, 2, 2, 2],
      [0, 2, 2, 1, 2, 2],
      [2, 2, 0, 2, 2, 2]]

s2 = [[2, 0, 1, 2, 2, 1, 0, 0, 2, 0],
      [2, 1, 0, 1, 2, 2, 0, 2, 0, 0],
      [0, 1, 2, 2, 2, 2, 1, 0, 2, 1],
      [2, 0, 1, 2, 2, 0, 2, 2, 1, 2],
      [1, 2, 2, 2, 1, 2, 0, 1, 0, 2],
      [0, 2, 2, 2, 2, 2, 0, 2, 2, 2],
      [2, 2, 2, 2, 2, 0, 2, 0, 0, 2],
      [2, 0, 0, 2, 0, 0, 2, 2, 2, 2],
      [0, 2, 0, 2, 2, 2, 2, 2, 1, 0],
      [0, 2, 1, 0, 2, 1, 1, 2, 1, 2]]

s3 = [[1, 0, 2, 2, 0, 2, 0, 0, 2, 0],
      [1, 2, 0, 1, 2, 2, 0, 2, 2, 2],
      [2, 1, 2, 2, 1, 2, 2, 2, 2, 1],
      [1, 2, 0, 2, 2, 2, 2, 2, 1, 2],
      [2, 2, 1, 2, 1, 0, 2, 2, 2, 2],
      [0, 2, 2, 2, 2, 2, 0, 2, 0, 2],
      [2, 2, 2, 2, 1, 2, 2, 2, 0, 1],
      [2, 2, 1, 2, 1, 2, 2, 2, 2, 2],
      [2, 1, 2, 2, 2, 2, 1, 2, 0, 2],
      [0, 0, 2, 0, 2, 1, 2, 2, 2, 1]]


def freiesfeld(S):
    for zeile in range(len(S)):
        for spalte in range(len(S[zeile])):
            if S[zeile][spalte] == 2:
                return zeile, spalte
    return -1, -1


def zulaessig(S, z, s):
    count_zero = 0
    count_one = 0
    for spalte in range(len(S[z]) - 2):
        if S[z][spalte] != 2:
            if S[z][spalte] == S[z][spalte + 1] == S[z][spalte + 2]:
                return False
    for zeile in range(len(S) - 2):
        if S[zeile][s] != 2:
            if S[zeile][s] == S[zeile + 1][s] == S[zeile + 2][s]:
                return False
    if z >= 1:
        if S[z - 1].count(0) != S[z - 1].count(1):
            return False
    if z == len(S[z]) - 1:
        for spalte in range(s + 1):
            for zeile in range(len(S)):
                if S[zeile][spalte] == 0:
                    count_zero += 1
                elif S[zeile][spalte] == 1:
                    count_one += 1
            if count_one != count_zero:
                return False
    return True


def vervollstaendigen(S):
    zeile, spalte = freiesfeld(S)
    if zeile == -1:
        for i in S:
            print(i)
        return True
    for i in range(2):
        S[zeile][spalte] = i
        if zulaessig(S, zeile, spalte):
            if vervollstaendigen(S):
                return True
        S[zeile][spalte] = 2
    return False


binairos = [s1, s2, s3]

for binairo in binairos:
    vervollstaendigen(binairo)
    print()
