# Implementieren Sie in dieser Datei das Programm, das zunächst eine Zahl als obere Schranke von der Konsole
# einliest. Im Anschluss soll ihr Programm alle Primzahlzwillinge bis zu dieser oberen Schranke finden und auf der
# Konsole ausgeben.

obere_schranke = int(input("Gebe eine Zahl als obere Schranke an: "))
aktuell = 2
list_prim = []
primpaar_list = []

while aktuell <= obere_schranke:
    teiler = 2

    while teiler * teiler <= aktuell:
        if aktuell % teiler == 0:
            break
        teiler += 1
    else:
        list_prim.append(aktuell)
        for i in list_prim:
            if aktuell - i == 2:
                primpaar = i, aktuell
                primpaar_list.append(primpaar)

    aktuell += 1

print("Die Primzahlzwillinge bis zur Zahl", obere_schranke, "sind: ")
print(*primpaar_list, sep=", ")