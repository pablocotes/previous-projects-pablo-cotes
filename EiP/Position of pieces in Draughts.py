def zulaessig(stellung):
    k = len(stellung)
    for spalte in range(k - 1):
        if stellung[spalte] == stellung[k - 1]:
            return False
        if (stellung[spalte] - stellung[k - 1]) == spalte - (k - 1):
            return False
        if (stellung[spalte] - stellung[k - 1]) == -(spalte - (k - 1)):
            return False
    return True

# This function finds possible positions where the pieces are not in the same row, column or diagonal
def vervollstaendige(n, stellung):
    falsch = 0
    anzahl = 0
    if len(stellung) == n:
        return 1
    for zeile in range(n):
        stellung.append(zeile)
        if zulaessig(stellung):
            x = vervollstaendige(n, stellung)
            if type(x) != bool:
                anzahl += x
                falsch = 1
        stellung.pop()
    if falsch == 0:
        return falsch
    return anzahl